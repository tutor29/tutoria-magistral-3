const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/MinShop')
    .then(console.log('Database is conected to MinShop'))
    .catch(err => console.log(`Database connection failed ${err}`))
    