const mongoose = require('mongoose');

const categorySchema = mongoose.Schema({
    name: String,
    typeProduct: String,
},
{
    versionKey: false,
});

module.exports = mongoose.model('Category', categorySchema);