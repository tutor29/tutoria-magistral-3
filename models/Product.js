const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    category: {
        ref: 'Category',
        type: mongoose.Types.ObjectId,
    }
});

module.exports = mongoose.model('Product', productSchema);