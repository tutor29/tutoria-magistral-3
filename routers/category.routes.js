const { Router } = require('express');
const Category = require('../models/Category');

const router = Router();

router.post('/', async (req, res) => {
    try {
        const { name, typeProduct } = req.body;
        const newCategory = new Category({
            name,
            typeProduct,
        })
        const savedCategory = await newCategory.save();
        res.json(savedCategory);
    } catch (error) {
        res.json(`Error: ${error}`);
    }
});

module.exports = router;