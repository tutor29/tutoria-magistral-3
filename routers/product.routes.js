const { Router } = require('express');
const Product = require('../models/Product');

const router = Router();

router.get('/', async (req, res) => {
    try {
        const products = await Product.find().populate('category');
        res.json(products);
    } catch (error) {
        res.send(error);
    }
});

router.post('/', async (req, res) => {
    try {
        const { name, price, category } = req.body;
        const newProduct = new Product({
            name,
            price,
            category,
        })
        const savedProduct = await newProduct.save();
        res.json(savedProduct);
    } catch (error) {
        res.json(`Error: ${error}`);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id).populate('category');
        res.json(product);
    } catch (error) {
        res.send(error);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const product = await Product.deleteOne({_id: req.params.id});
        res.json(product);
    } catch (error) {
        res.send(error);
    }
});

module.exports = router;