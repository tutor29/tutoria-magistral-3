const express = require('express');
const productRoutes = require('./routers/product.routes');
const categoryRoutes = require('./routers/category.routes');
require('./database')
const app = express();
app.use(express.json());
app.use('/products', productRoutes);
app.use('/categories', categoryRoutes);

app.listen(4000, () => {
    console.log('Server is running on port 4000');
})